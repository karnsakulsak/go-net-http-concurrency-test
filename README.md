## Objective
Test concurrency of HTTP server created by net/http module of Go

## Pre-requisite
* Go compiler (I have tested on go1.8.2 windows/amd64)

## How to run server
* Clone this repository
* Run with command
```
go run go-net-http-concurrency.go
```

## How to test
* Run apache bench (or other command that can simutaneously fire request to our server) in same or other machine
```
ab -n 10 -c 10 http://<URL to server>
```