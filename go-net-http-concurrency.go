package main

import (
	"fmt"
	"net/http"
	"time"
	"sync"
)

func main() {
	println("Go net/http concurrency test server version 0.0.1-7")

	var requestIndex = 1
	var mutex = &sync.Mutex{}

	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		mutex.Lock()
		localIndex := requestIndex
		requestIndex++
		mutex.Unlock()

		fmt.Printf("--START:%05d:%s\n", localIndex, time.Now().Format(time.RFC3339Nano))
		
		time.Sleep(10 * 1000 * time.Millisecond)
		w.Header().Set("Content-Type", "text/plain")
		w.Write([]byte(fmt.Sprintf("%05d\n", localIndex)))
		
		fmt.Printf("-FINISH:%05d:%s\n", localIndex, time.Now().Format(time.RFC3339Nano))
	})

	http.ListenAndServe(":8000", nil)

}